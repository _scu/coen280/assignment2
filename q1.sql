SELECT P.name
FROM MOVIE M, PERSON P, ROLES R
WHERE M.MID = R.Movie AND P.PID = R.Person AND M.title = 'The Da Vinci Code'
ORDER BY P.name ASC;