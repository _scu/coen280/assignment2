-- a
CREATE VIEW High_Ratings AS
	SELECT DISTINCT P.PID, P.Name
	FROM PERSON P, ROLES R, REVIEWS Re
	WHERE P.PID = R.Person AND R.Movie = Re.Movie AND Re.Rating >= 8;

CREATE VIEW Low_Ratings AS
	SELECT DISTINCT P.PID, P.Name
	FROM PERSON P, ROLES R, REVIEWS Re
	WHERE P.PID = R.Person AND R.Movie = Re.Movie AND Re.Rating < 8;

-- a.a
SELECT count(*)
FROM High_Ratings;

-- a.b
SELECT count(*)
FROM Low_Ratings;

-- b

SELECT count(*)
FROM (
	SELECT * from High_Ratings
	MINUS
	SELECT * from Low_Ratings
);

-- c
CREATE VIEW No_Flop AS
	SELECT * from High_Ratings
	MINUS
	SELECT * from Low_Ratings;

SELECT *
FROM (
	SELECT P.Name, count (M.MID) AS Count
	FROM No_Flop NF, PERSON P, ROLES R, MOVIE M
	WHERE P.pid = R.Person AND P.PID = NF.PID AND M.MID = R.Movie
	GROUP BY P.name
	ORDER BY Count DESC, P.name ASC
)
WHERE ROWNUM <= 10;

-- Clean up
DROP VIEW No_Flop;
DROP VIEW High_Ratings;
DROP VIEW Low_Ratings;