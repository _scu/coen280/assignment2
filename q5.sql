SELECT s1.Title, s1.Count as "cast size"
FROM(
	SELECT M.Title, s.Count, RANK() OVER (ORDER BY s.Count DESC) rank
	FROM(
		SELECT R.Movie, count(DISTINCT R.Person) AS Count
		FROM ROLES R
		GROUP BY R.Movie
	) s, MOVIE M
	WHERE M.MID = s.Movie
) s1
WHERE rank = 1;