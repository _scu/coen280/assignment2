echo -n "Username: "
read USERNAME
echo -n "Password: "
read -s PASSWORD
echo
echo -n "database Name: "
read DB_NAME

EXEC_CMD='sqlplus $USERNAME/$PASSWORD@$DB_NAME < '

`$EXEC_CMD` createdb.sql

for query in $(ls q*.sql)
do
	`$EXEC_CMD` $query
done

`$EXEC_CMD` dropdb.sql