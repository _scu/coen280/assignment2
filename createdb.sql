-- CREATE Tables

CREATE TABLE Person(
	PID VARCHAR(5) NOT NULL,
	Name VARCHAR(40) NOT NULL,
	Birthdate DATE,
	Gender CHAR(1),
	Birthplace VARCHAR(15),
	Attribute VARCHAR(8),
	PRIMARY KEY(PID)
);

CREATE TABLE IMDB_User(
	IMDB_ID VARCHAR(6) NOT NULL,
	Email VARCHAR(30) NOT NULL,
	First_Name VARCHAR(20) NOT NULL,
	Last_Name VARCHAR(20),
	DOB DATE,
	Birthplace VARCHAR(15),
	Gender CHAR(1),
	PRIMARY KEY(IMDB_ID)
);

CREATE TABLE Movie(
	MID VARCHAR(5) NOT NULL,
	Title VARCHAR(60) NOT NULL,
	Release_Year SMALLINT,
	Director VARCHAR(5) NOT NULL,
	PRIMARY KEY(MID)
);

CREATE TABLE Movie_Genre(
	MID VARCHAR(5) NOT NULL,
	Genre VARCHAR(10) NOT NULL,
	FOREIGN KEY(MID) REFERENCES Movie(MID) ON DELETE CASCADE
);

CREATE TABLE Roles(
	Movie VARCHAR(5) NOT NULL,
	Person VARCHAR(5) NOT NULL,
	Role VARCHAR(40) NOT NULL,
	FOREIGN KEY(Movie) REFERENCES Movie(MID) ON DELETE CASCADE,
	FOREIGN KEY(Person) REFERENCES Person(PID) ON DELETE CASCADE
);

CREATE TABLE Reviews(
	Movie VARCHAR(5) NOT NULL,
	Author VARCHAR(6) NOT NULL,
	Rating SMALLINT CHECK(Rating >= 0 AND Rating <= 10),
	Votes SMALLINT CHECK(Votes >= 0),
	Publish_Date VARCHAR(24),
	FOREIGN KEY(Movie) REFERENCES Movie(MID) ON DELETE CASCADE,
	FOREIGN KEY(Author) REFERENCES IMDB_User(IMDB_ID) ON DELETE CASCADE
);

-- POPULATE Data

INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P1', 'Brian de forma', TO_DATE('1940-09-11', 'yyyy-mm-dd'), 'M', 'New York', 'Director');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P2', 'Martin Brest', TO_DATE('1951-08-08', 'yyyy-mm-dd'), 'M', 'San Jose', 'Director');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P3', 'Scarlett Johanson', TO_DATE('1984-11-22', 'yyyy-mm-dd'), 'F', 'Austin', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P4', 'Luc Besson', TO_DATE('1975-05-30', 'yyyy-mm-dd'), 'F', 'Paris', 'Director');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P5', 'Morgan Freeman', TO_DATE('1953-06-05', 'yyyy-mm-dd'), 'M', 'Canberra', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P6', 'Al Pacino', TO_DATE('1956-11-12', 'yyyy-mm-dd'), 'M', 'Portland', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P7', 'Angelina Jolie', TO_DATE('1970-03-03', 'yyyy-mm-dd'), 'F', 'Seattle', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P8', 'Brad Pitt', TO_DATE('1975-04-04', 'yyyy-mm-dd'), 'M', 'London', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P9', 'Tom Hanks', TO_DATE('1964-05-19', 'yyyy-mm-dd'), 'M', 'Perth', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P10', 'Jessica Alba', TO_DATE('1983-08-07', 'yyyy-mm-dd'), 'F', 'Seoul', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P12', 'Alex Parish', TO_DATE('1977-07-09', 'yyyy-mm-dd'), 'F', 'San Jose', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P13', 'Jack Nicholson', TO_DATE('1958-11-13', 'yyyy-mm-dd'), 'M', 'Austin', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P15', 'Harrison Ford', TO_DATE('1957-09-11', 'yyyy-mm-dd'), 'M', 'Canberra', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P16', 'Julia Roberts', TO_DATE('1967-01-01', 'yyyy-mm-dd'), 'F', 'Portland', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P17', 'Matt Damon', TO_DATE('1971-01-07', 'yyyy-mm-dd'), 'M', 'Seattle', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P18', 'Jennifer Lawrence', TO_DATE('1962-02-02', 'yyyy-mm-dd'), 'F', 'London', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P19', 'George clooney', TO_DATE('1965-03-03', 'yyyy-mm-dd'), 'M', 'Perth', 'Actor');
INSERT INTO Person(PID, Name, Birthdate, Gender, Birthplace, Attribute) VALUES('P20', 'Jennifer Aniston', TO_DATE('1968-04-04', 'yyyy-mm-dd'), 'F', 'Seoul', 'Actor');

INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID1', 'john@yahoo.com', 'John', 'Smith', TO_DATE('1995-10-05', 'yyyy-mm-dd'), 'FL', 'M');
INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID2', 'juan@gmail.com', 'Juan', 'Carlos', TO_DATE('1994-04-12', 'yyyy-mm-dd'), 'AK', 'M');
INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID3', 'Jane@gmail.com', 'Jane', 'Chapel', TO_DATE('1993-11-02', 'yyyy-mm-dd'), 'IL', 'F');
INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID4', 'adi@yahoo.com', 'Aditya', 'Awasthi', TO_DATE('1992-12-12', 'yyyy-mm-dd'), 'CA', 'M');
INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID5', 'james@hotmail.com', 'James', 'Williams', TO_DATE('1991-05-05', 'yyyy-mm-dd'), 'NY', 'M');
INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID6', 'mike@yahoo.com', 'Mike', 'Brown', TO_DATE('1988-03-01', 'yyyy-mm-dd'), 'NC', 'M');
INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID7', 'bob@yahoo.com', 'Bob', 'Jones', TO_DATE('1988-02-07', 'yyyy-mm-dd'), 'NY ', 'M');
INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID8', 'wei@gmail.com', ' Wei', 'Zhang', TO_DATE('1985-08-12', 'yyyy-mm-dd'), 'NV', 'F');
INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID9', 'mark@gmail.com', 'Mark', 'Davis', TO_DATE('1984-05-10', 'yyyy-mm-dd'), 'CA', 'M');
INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID10', 'daniel@yahoo.com', 'Daniel ', 'Garcia', TO_DATE('1980-06-01', 'yyyy-mm-dd'), 'NJ', 'M');
INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID11', 'maria@hotmail.com', 'Maria', 'Rodriguez', TO_DATE('1975-03-18', 'yyyy-mm-dd'), 'CA', 'F');
INSERT INTO IMDB_User(IMDB_ID, Email, First_Name, Last_Name, DOB, Birthplace, Gender) VALUES('ID12', 'freya@yahoo.com', 'Freya', 'Wilson', TO_DATE('1970-02-19', 'yyyy-mm-dd'), 'NJ', 'F');

INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M1', 'Scarface', 1988, 'P1');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M2', 'Scent of a women', 1995, 'P2');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M3', 'My big fat greek wedding', 2000, 'P4');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M4', 'The Devil''s Advocate', 1997, 'P1');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M5', 'Mr. and Mrs Smith', 1965, 'P1');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M6', 'Now You see me', 2013, 'P2');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M7', 'Barely Lethal', 2014, 'P4');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M8', 'The Man with one red shoe', 1984, 'P1');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M9', 'The Polar Express', 2010, 'P2');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M10', 'Her', 2013, 'P2');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M11', 'Lucy', 2015, 'P4');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M12', 'The Da Vinci Code', 2005, 'P4');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M13', 'The God Father part II', 1975, 'P1');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M15', 'Angels and Daemons', 2009, 'P2');
INSERT INTO Movie(MID, Title, Release_Year, Director) VALUES('M16', 'The Island', 2010, 'P4');

INSERT INTO Movie_Genre(MID, Genre) VALUES('M1', 'Action');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M2', 'Action');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M2', 'Comedy');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M3', 'Comedy');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M4', 'Thriller');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M5', 'Comedy');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M5', 'Action');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M6', 'Thriller');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M7', 'Action');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M8', 'comedy');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M9', 'comedy');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M10', 'Thriller');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M11', 'Thriller');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M12', 'Action');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M12', 'Thriller');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M13', 'Action');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M13', 'Thriller');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M15', 'Action');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M15', 'Thriller');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M16', 'Action');
INSERT INTO Movie_Genre(MID, Genre) VALUES('M16', 'Comedy');

INSERT INTO Roles(Movie, Person, Role) VALUES('M1', 'P5', 'Jessica');
INSERT INTO Roles(Movie, Person, Role) VALUES('M1', 'P6', 'robert');
INSERT INTO Roles(Movie, Person, Role) VALUES('M2', 'P5', 'John');
INSERT INTO Roles(Movie, Person, Role) VALUES('M2', 'P6', 'Mark');
INSERT INTO Roles(Movie, Person, Role) VALUES('M3', 'P9', 'Alex');
INSERT INTO Roles(Movie, Person, Role) VALUES('M3', 'P7', 'Julie');
INSERT INTO Roles(Movie, Person, Role) VALUES('M4', 'P5', 'Jessica');
INSERT INTO Roles(Movie, Person, Role) VALUES('M4', 'P6', 'Matt');
INSERT INTO Roles(Movie, Person, Role) VALUES('M4', 'P8', 'Jennifer');
INSERT INTO Roles(Movie, Person, Role) VALUES('M5', 'P7', 'Jennifer');
INSERT INTO Roles(Movie, Person, Role) VALUES('M5', 'P8', 'Tom');
INSERT INTO Roles(Movie, Person, Role) VALUES('M5', 'P5', 'Milo');
INSERT INTO Roles(Movie, Person, Role) VALUES('M6', 'P6', 'Chris');
INSERT INTO Roles(Movie, Person, Role) VALUES('M6', 'P7', 'Rose');
INSERT INTO Roles(Movie, Person, Role) VALUES('M6', 'P5', 'Bill');
INSERT INTO Roles(Movie, Person, Role) VALUES('M7', 'P10', 'Jane');
INSERT INTO Roles(Movie, Person, Role) VALUES('M7', 'P5', 'Brad');
INSERT INTO Roles(Movie, Person, Role) VALUES('M8', 'P9', 'Lucas');
INSERT INTO Roles(Movie, Person, Role) VALUES('M8', 'P10', 'Juanita');
INSERT INTO Roles(Movie, Person, Role) VALUES('M9', 'P9', 'Clayne');
INSERT INTO Roles(Movie, Person, Role) VALUES('M9', 'P9', 'Jane');
INSERT INTO Roles(Movie, Person, Role) VALUES('M9', 'P9', 'Brad');
INSERT INTO Roles(Movie, Person, Role) VALUES('M9', 'P9', 'Lucas');
INSERT INTO Roles(Movie, Person, Role) VALUES('M9', 'P9', 'Bradley');
INSERT INTO Roles(Movie, Person, Role) VALUES('M9', 'P9', 'Justin');
INSERT INTO Roles(Movie, Person, Role) VALUES('M9', 'P17', 'Martin');
INSERT INTO Roles(Movie, Person, Role) VALUES('M9', 'P19', 'Mike');
INSERT INTO Roles(Movie, Person, Role) VALUES('M10', 'P3', 'Travis');
INSERT INTO Roles(Movie, Person, Role) VALUES('M10', 'P5', 'Alexander');
INSERT INTO Roles(Movie, Person, Role) VALUES('M10', 'P6', 'Justin');
INSERT INTO Roles(Movie, Person, Role) VALUES('M11', 'P3', 'Jessica');
INSERT INTO Roles(Movie, Person, Role) VALUES('M11', 'P5', 'Johnny');
INSERT INTO Roles(Movie, Person, Role) VALUES('M11', 'P8', 'Rami');
INSERT INTO Roles(Movie, Person, Role) VALUES('M11', 'P9', 'Sam');
INSERT INTO Roles(Movie, Person, Role) VALUES('M12', 'P9', 'Gatek');
INSERT INTO Roles(Movie, Person, Role) VALUES('M12', 'P10', 'Rose');
INSERT INTO Roles(Movie, Person, Role) VALUES('M12', 'P3', 'maria');
INSERT INTO Roles(Movie, Person, Role) VALUES('M13', 'P5', 'Travis');
INSERT INTO Roles(Movie, Person, Role) VALUES('M13', 'P6', 'Alexander');
INSERT INTO Roles(Movie, Person, Role) VALUES('M13', 'P16', 'Pearl');
INSERT INTO Roles(Movie, Person, Role) VALUES('M15', 'P12', 'Sofia');
INSERT INTO Roles(Movie, Person, Role) VALUES('M15', 'P18', 'chrissy');
INSERT INTO Roles(Movie, Person, Role) VALUES('M15', 'P9', 'Mike');
INSERT INTO Roles(Movie, Person, Role) VALUES('M16', 'P10', 'Martin');
INSERT INTO Roles(Movie, Person, Role) VALUES('M16', 'P15', 'Bill');
INSERT INTO Roles(Movie, Person, Role) VALUES('M16', 'P16', 'Emilia');

INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M1', 'ID1', 7, 25, '2007-10-02 09:10:54');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M2', 'ID2', 8, 35, '2007-09-29 13:45:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M2', 'ID3', 9, 24, '2007-09-29 10:38:25');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M3', 'ID4', 10, 8, '2013-10-02 13:05:56');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M3', 'ID5', 9, 11, '2007-10-25 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M4', 'ID6', 8, 6, '2007-09-26 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M4', 'ID7', 7, 23, '2007-09-26 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M5', 'ID9', 9, 22, '2007-09-28 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M6', 'ID10', 8, 26, '2007-10-29 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M7', 'ID11', 8, 27, '2007-09-30 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M7', 'ID12', 8, 18, '2007-10-25 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M8', 'ID1', 7, 19, '2007-09-25 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M9', 'ID2', 7, 16, '2007-09-25 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M10', 'ID3', 8, 18, '2007-09-29 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M11', 'ID4', 9, 22, '2015-06-07 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M11', 'ID5', 10, 13, '2015-05-05 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M12', 'ID6', 9, 50, '2015-05-05 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M13', 'ID7', 5, 34, '2007-10-25 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M13', 'ID1', 4, 34, '2007-10-25 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M15', 'ID10', 8, 25, '2015-05-05 17:15:00');
INSERT INTO Reviews(Movie, Author, Rating, Votes, Publish_Date) VALUES('M16', 'ID11', 7, 12, '2015-05-05 17:15:00');