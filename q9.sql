WITH al_pacino AS (
	SELECT P.PID
	FROM PERSON P
	WHERE P.Name = 'Al Pacino'
),
actor_list AS (
	SELECT DISTINCT R.Person, count(R.Movie) AS count
	FROM ROLES R, (
		SELECT DISTINCT R.Movie
		FROM al_pacino ap, ROLES R
		WHERE R.Person = ap.PID
	) movie_list
	WHERE R.Movie in movie_list.Movie
	GROUP BY R.Person
) 
SELECT P.Name 
FROM PERSON P
WHERE P.PID in (
	SELECT al.PERSON
	FROM actor_list al, al_pacino ap2
	WHERE al.PERSON != ap2.PID and al.count = (
		SELECT count
		FROM actor_list al1, al_pacino ap1
		WHERE al1.PERSON = ap1.PID
	)
);

-- Old query
-- SELECT DISTINCT P.name as co_actors
-- FROM PERSON P, ROLES R
-- WHERE P.PID = R.Person AND R.Movie = ALL(
-- 	SELECT M.MID
-- 	FROM MOVIE M, ROLES R2, PERSON P2
-- 	WHERE M.MID = R2.Movie AND P2.PID = R2.Person AND P2.name = 'Al Pacino'
-- );