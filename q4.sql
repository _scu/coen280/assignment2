SELECT s1.decade
FROM(
	SELECT s.decade, RANK() OVER (ORDER BY s.Count DESC) rank
	FROM(
		SELECT (FLOOR(M.Release_Year/10) * 10) AS decade, COUNT(M.mid) AS Count
		FROM MOVIE M
		GROUP BY FLOOR(M.Release_Year/10)
	) s
) s1
WHERE rank = 1;