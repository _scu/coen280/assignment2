SELECT DISTINCT s.Release_Year, M2.Title, s.Rating
FROM (
	SELECT M1.Release_Year, max(R1.Rating) as Rating
		FROM MOVIE M1, REVIEWS R1
		WHERE M1.MID = R1.Movie AND M1.Release_Year >= 2005
		GROUP BY M1.Release_Year
	) s, MOVIE M2, REVIEWS R2
WHERE M2.MID = R2.Movie AND M2.Release_Year = s.Release_Year AND R2.Rating = s.Rating
ORDER BY s.Release_Year ASC, M2.Title ASC;