SELECT DISTINCT M.MID, M.title, M.Release_Year, s1.number_of_ratings as "Num Ratings", R.Rating
FROM MOVIE M, REVIEWS R, (
	SELECT Re.Movie, count(Re.Rating) as number_of_ratings
	FROM REVIEWS Re
	GROUP BY Re.Movie
) s1
WHERE M.MID = R.Movie AND M.MID = s1.Movie AND R.Rating = (
	SELECT max(Re2.Rating)
	FROM REVIEWS Re2
)
ORDER BY LENGTH(M.MID) ASC, M.MID ASC;