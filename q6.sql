SELECT P.name as actor, M.title as movie, count(DISTINCT R.Role) as distinct_roles
FROM ROLES R, PERSON P, MOVIE M
WHERE M.MID = R.Movie AND P.PID = R.Person AND M.Release_Year = 2010
GROUP BY P.name, M.Title
HAVING count(DISTINCT R.Role) >= 5;